package com.lelin.baseproject.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.lelin.baseproject.R
import com.lelin.baseproject.utils.Status
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val mainViewModel:MainViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel.populars.observe(this, Observer {
            when(it.status){
                Status.LOADING->{
                    Log.e("TAG", "onCreate: Loading list" )
                }
                Status.SUCCESS->{
                    Log.e("TAG", "onCreate:  list :"+it.data.toString() )

                }
                Status.ERROR->{
                    Log.e("TAG", "onCreate: Error List list" )

                }
            }
        })
    }
}