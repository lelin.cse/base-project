package com.lelin.baseproject.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lelin.baseproject.data.model.Popular
import com.lelin.baseproject.data.repository.Repository
import com.lelin.baseproject.utils.NetworkHelper
import com.lelin.baseproject.utils.Resource
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(val repository: Repository,val networkHelper: NetworkHelper) :ViewModel() {

    private val _populars=MutableLiveData<Resource<List<Popular>>>()

    val populars:LiveData<Resource<List<Popular>>>
    get() = _populars

    init {
        fetchPopular()
    }

    fun fetchPopular(){
        viewModelScope.launch {
            _populars.postValue(Resource.loading(null))

            if (networkHelper.isNetworkConnected()){
                repository.getPopular().let {
                    if (it.isSuccessful){
                        _populars.postValue(Resource.success(it.body()))
                    }
                    else
                        _populars.postValue(Resource.error(it.errorBody().toString(),null))
                }
            }
            else
                _populars.postValue(Resource.error("No Internet Connection",null))
        }
    }
}