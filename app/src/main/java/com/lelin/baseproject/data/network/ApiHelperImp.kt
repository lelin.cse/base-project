package com.lelin.baseproject.data.network

import com.lelin.baseproject.data.model.Popular
import com.lelin.baseproject.utils.Resource
import javax.inject.Inject

class ApiHelperImp @Inject constructor(private val apiService: ApiService) :ApiHelper {
    override suspend fun getPopular() = apiService.getPopular()
}