package com.lelin.baseproject.data.network

import com.lelin.baseproject.data.model.Popular
import com.lelin.baseproject.utils.Resource
import retrofit2.Response

interface ApiHelper {
    suspend fun getPopular():Response<List<Popular>>
}