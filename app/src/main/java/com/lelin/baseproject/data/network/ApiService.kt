package com.lelin.baseproject.data.network

import com.lelin.baseproject.data.model.Popular
import com.lelin.baseproject.utils.Resource
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET("episodes/popular/0/10")
    suspend fun getPopular():Response<List<Popular>>
}