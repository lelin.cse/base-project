package com.lelin.baseproject.data.repository

import com.lelin.baseproject.data.network.ApiHelper
import com.lelin.baseproject.data.network.ApiHelperImp
import javax.inject.Inject

class Repository @Inject constructor(private val apiHelper: ApiHelper){

    suspend fun getPopular()=apiHelper.getPopular()
}