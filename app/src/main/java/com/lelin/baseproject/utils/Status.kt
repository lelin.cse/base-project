package com.lelin.baseproject.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}